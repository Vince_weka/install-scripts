#!/bin/bash

DESCRIPTION="Check passwordless ssh to all hosts..."
source ../preamble


# Put your stuff here

let ERRORS=0
#
# check ssh connectivity, if given hostnames/ips on command line
#
if [ $# -gt 0 ]; then
	for i in $*
	do
		ssh -o PasswordAuthentication=no  -o BatchMode=yes -o StrictHostKeyChecking=no $i exit &>/dev/null
		if [ $? -eq 0 ];
		then
			write_log "Host $i ssh test passed"
		else
			write_log "Host $i ssh test failed"
			let ERRORS=$ERRORS+1
		fi
	done
else
	echo "No hosts specified, skipping ssh connectivity test."
fi

write_log "There were $ERRORS failures"

if [ $ERRORS -gt 0 ]; then
	ret=255		# if we can't ssh to all the servers, we can't continue
fi

source ../postscript
