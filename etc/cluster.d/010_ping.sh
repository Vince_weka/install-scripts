#!/bin/bash

DESCRIPTION="Basic IP connectivity tests with Jumbo Frames"
source ../preamble


# Put your stuff here

let ERRORS=0
#
# check ssh connectivity, if given hostnames/ips on command line
#
if [ $# -gt 0 ]; then
	for i in $*
	do
		# check for jumbo frames working corectly as well as basic connectivity.
		PINGOUT=`ping -M do -c 20 -i 0.2 -q -s 4092  $i | tail -2 | head -1`
		echo $PINGOUT | grep errors
		if [ $? -eq 0 ];
		then
			write_log "Host $i ping error. failed."
			let ERRORS=$ERRORS+1
		else
			LOSS=`echo $PINGOUT | awk -F, '{ print $3 }' | cut '-d%' -f1`
			if [ $LOSS -gt 0 ]; then
				write_log "Host $i has ${LOSS}% packet loss. failed"
				let ERRORS=$ERRORS+1
			else
				write_log "Host $i ping test passed."
			fi
		fi
	done
else
	echo "No hosts specified, skipping ssh connectivity test."
fi

write_log "There were $ERRORS failures"

if [ $ERRORS -gt 0 ]; then
	ret=255		# if we can't ping all the servers, we can't continue
fi

source ../postscript
