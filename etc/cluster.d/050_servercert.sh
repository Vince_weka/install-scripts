#!/bin/bash

DESCRIPTION="Execute server-cert on all nodes..."
source ../preamble


# Execute the server-cert.sh on all nodes
ret="0"
for i in $*
do
	write_log "Testing $i"
	ssh $i ${PWD%etc/cluster.d}/server-cert.sh --remote
	if [ $? -ne 0 ]; then
		write_log "server-cert on host $i failed"
		ret="1"
	else
		write_log "server-cert on host $i passed"
	fi
done

source ../postscript
