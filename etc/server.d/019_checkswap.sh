#!/bin/bash

DESCRIPTION="Checking Linux swap status"
source ../preamble


swap_state=`swapon -s`

if [ -z "$swap_state" ]; then
	write_log "Swap cache is disabled"
	ret="0"
else
	write_log "Swap cache is enabled on this system, general suggestion would be disabling the cache by issuing: swapoff -a"
	ret="1"
fi

source ../postscript
