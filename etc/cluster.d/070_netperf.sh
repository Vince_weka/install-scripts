#!/bin/bash
# Example script included to run specific test

# Return codes are as follows: 0 = Success, >0 = Failure, 255 = Fatal failure (stop all tests)

DESCRIPTION="Run iperf to all nodes"
source ../preamble

# pdsh needs a comma-separated list of hosts
pdsh_hosts=`echo $* | sed 's/ /,/g'`

echo Host list is $pdsh_hosts

# use awk for floating point math, because it's always installed.  bc is an alternative, but optional command

for i in $*; do
	INTERFACE=`ip route get $i | awk '{ print $3 }'` 
	NETSPEED=`ethtool $INTERFACE | grep Speed | awk '{print $2}'`
	NETSPEED=${NETSPEED%Mb/s}
	NETGB=`echo $NETSPEED | awk '{ print $1 / 1000}'`
	GOOD=`echo $NETSPEED | awk '{ print $1 * 0.9}'`   # 90% of max is reasonable?
	write_log "Link is $NETSPEED Mbits/s or $NETGB Gbits/s"
	write_log "starting iperf client to node $i"
	MBITS=`iperf -c $i -P 50 -f m | tail -1 | awk '{print $6}'`
	GBITS=`echo $MBITS | awk '{ print $1 / 1000}'`
	write_log "node $i, $GBITS Gbits/s"
	if [ $MBITS -lt $GOOD ]; then		# can't do floating point in bash, but we can bypass that
		write_log "Insufficient bandwidth to node $i detected"
		ret=1
	fi
done

write_log "iperfs complete!"

source ../postscript
