#!/bin/bash

DESCRIPTION="Check OS Release..."
source ../preamble

# Check OS version if there is redhat release file, if not, then lsb check, if no lsb, then hostnamectl
which hostnamectl &> /dev/null
if [ $? -eq 1 ]; then
	write_log "Could not find hostnamectl utility, unable to check OS version properly"
	ret="1"
else
	dist=`hostnamectl | grep -i operating | awk {'print $3'}`
	osver=`hostnamectl | grep -i operating | awk -F: {'print $2" "$3" "$4'} | sed 's/ //g' | sed 's/[a-zA-Z ]//g' | sed 's/()//g'`
	if [ -z $osver ]; then
		write_log "OS release number could not be detected, setting it as 0"
		osver="0"
	fi
	
	case $dist in
		red*|cent*|Cent*|Red* ) # need to get proper version running
			if [ ! -f /etc/redhat-release ]; then
				osver="0"
			else
				osver=`cat /etc/redhat-release | awk {'print $4'}`
			fi
			;;
	esac

	# Got some dist and osver strings in
	if [ -z $dist ] && [ -z $osver ]; then
        	echo "Could not find Dist or OS version running"
        	ret="1"
	else
        	# Checking if the version and OS are supported by Weka.IO requirements
        	check_dist=`echo $dist | sed 's/[a-zA-Z]/\L&/g'`
        	check_osver=`echo $osver | sed 's/[a-zA-Z ]//g'`
        	case $check_dist in
			debian) case $check_osver in
				9.7* | 9.8* ) write_log "OS $check_dist and version $check_osver are supported"
					ret="0"
					;;
				*) write_log "OS $check_dist and version $check_osver are not supported"
					ret="1"
					;;
			esac
                        ;;
			red*|cent*) case $check_osver in
				6.8* | 6.9* | 6.10* | 7.2* | 7.3* | 7.4* | 7.5* | 7.6* ) write_log "OS $check_dist and version $check_osver are supported"
					ret="0"
					;;
				*) write_log "OS $check_dist and version $check_osver are not supported"
					ret="1"
					;;
			esac
			;;
			aws*|amazon*|Amazon*) case $check_osver in
				1703 | 1709 | 1712 | 1803 | 2 | Linux ) write_log "OS $check_dist and version $check_osver are supported"
					ret="0"
					;;
				*) write_log "OS $check_dist and version $check_osver are not supported"
					ret="1"
					;;
			esac
			;;
			ubuntu*) case $check_osver in
				16* | 18* ) write_log "OS $check_dist and version $check_osver are supported"
					ret="0"
					;;
				*) write_log "OS $check_dist and version $check_osver are not supported"
					ret="1"
					;;
			esac
			;;
			*) write_log "OS type: $check_dist. This version of OS is currently unsupported by Weka.IO"
				ret="1"
				;;
        	esac
	fi
fi

source ../postscript
