#!/bin/bash

DESCRIPTION="Checking if Numa balancing is enabled"
source ../preamble

# Put your stuff here
if [ ! -e /proc/sys/kernel/numa_balancing ]; then
	write_log "Could not find numa_balancing kernel entry in proc..."
	ret="1"
	return
else
	ret="0"
fi
numa_set=`cat /proc/sys/kernel/numa_balancing`
if [ "$numa_set" -eq "1" ]; then
	write_log "Numa balancing is enabled in the current running kernel configuration, it is generally recommended to disable this setting by entering the following command"
	write_log "echo 0 > /proc/sys/kernel/numa_balancing && echo "kernel.numa_blanacing=0" >> /etc/sysctl.conf"
	ret="1"
	return
else
	write_log "Numa balancing is disabled"
	ret="0"
fi

source ../postscript
