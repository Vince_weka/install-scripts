#!/bin/bash


DESCRIPTION="Checking for watchdog timer"
source ../preamble

# Checking for watchdog timer
if [ -c /dev/watchdog ]; then 
	write_log "Watchdog device exists"
	ret="0"
else
	write_log "Watchdog device does not exist"
	ret="1"
fi	
source ../postscript
