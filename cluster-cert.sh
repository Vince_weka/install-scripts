#!/bin/bash
# Used to start the test and/or configuration tasks as specified in etc/cluster.d folder

# puts is in the executable's directory, if we're not there
pushd `dirname $0` &> /dev/null	

source etc/parameters.conf
source etc/functions

# See if they gave servers names or ips as parameters
if [ $# -le 0 ]; then
	echo "Usage: $0 <list of hosts>"
	echo "	<list of hosts> can be specified 10.1.2.1{01..15} or weka{1..15}"
	echo "	or as a space-separated list: weka1 weka2 weka3..."
	exit 1
fi

exec_test () {

# Test include and run
ls etc/cluster.d/[0-9]*.sh &> /dev/null
if [ $? -eq 1 ]; then
	write_log "Could not find any test to perform in etc/cluster.d/ folder"
	exit 1
fi
pushd etc/cluster.d/ &> /dev/null
for i in [0-9]*; do
	./${i} $*
	RET=$?
	if [ $RET -gt 0 ]; then
		write_log "[\e[31m FAIL \e[0m]"
		write_log "=================================================="
		if [ $RET -eq 255 ]; then
			write_log "FATAL ERROR - tests cannot continue until issue is resolved.  Terminating tests."
			exit 1
		fi
	else
		write_log "[\e[32m SUCCESS \e[0m]"
		write_log "=================================================="
	fi
done
popd &> /dev/null

}

run_test () {
# If test scripts are enabled, it would run all the scripts is numeric order inside test folder
# if script doesn't have a number or not executable, it would not run

if [ "$test_enabled" == "true" ]; then
	write_log "Executing Cluster tests"
	exec_test $*
else
	write_log "Host tests are disabled, skipping"
fi
}

run_update () {
# This would perform update of the current script
update_site="198.24.47.125"
update_file="server-certification.tgz"

write_log "Performing update"

curl $update_site/$update_file --output $update_file &> /dev/null
if [ $? -ne 0 ]; then
	write_log "Could not reach $update_site for fetching the update file"
	return 1
else
	file $update_file | grep -vi html &> /dev/null
	if [ $? -eq 0 ]; then
		write_log "Update file $update_file fetched successfully, performing update"
		mv $update_file ../../
		pushd ../../ &> /dev/null
		tar xfz $update_file
		rm $update_file
		popd
		write_log "Update is now complete we are at version: `running_version`"
else
		write_log "Unfortunately, could not find proper update file"
		rm $update_file
		return 1
	fi
fi


}

set_conf () {
# Will set conf_enabled="false" to conf_enabled="true" or conf_enabled="false" and perform execution of the script from that point 
write_log "Setting configuration enabled to $1"

case $1 in 
	enabled  ) sed -i 's/\bconf_enabled=[^ ]*/conf_enabled="true"/' ../etc/parameters.conf;;
	disabled ) sed -i 's/\bconf_enabled=[^ ]*/conf_enabled="false"/' ../etc/parameters.conf;;
esac
source ../etc/parameters.conf
}

### MAIN ###
if [ `whoami` != "root" ]; then
	echo "It is commonly suggested to run this script as a user with root permissions"
	echo "Try sudo $0" 
	exit 1
fi


case $1 in
	--help | -help | -h | --h 				) echo "$0 version: `running_version` as root user, with --help / -h or --update / -u to update application (internet connection required) --conf / -c for perform configuration of OS"
								  exit 0
								  ;;
	--update | -update | --u | -u 				) run_update
								  exit 0
								  ;;
	--conf | -conf | --c | -c | -configure | --configure 	) 
								  set_conf enabled
								  run_test
								  run_conf
								  popd &> /dev/null
								  exit 0
								  ;;
esac
#set_conf disabled
run_test $*
#run_conf
# jump back to original dir, if different that the one we're in
popd &> /dev/null

