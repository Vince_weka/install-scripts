#!/bin/bash
# Example script included to run specific test

# Return codes are as follows: 0 = Success, >0 = Failure, 255 = Fatal failure (stop all tests)

DESCRIPTION="Start an iperf server so we can test network bandwidth later"
source ../preamble

# Put your stuff here
pkill iperf	# make sure it's not already running
(iperf -s &> /dev/null) &
write_log "iperf server started"
ret=0

source ../postscript
