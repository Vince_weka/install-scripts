#!/bin/bash

if [ $# -eq 0 ]; then
	echo "a list of hosts must be specified on the command line"
	exit 255
fi

DESCRIPTION="Distributing tests to all nodes."
source ../preamble

TMPDIR=/tmp/weka-install
mkdir -p $TMPDIR &> /dev/null

# Copy these scripts to all nodes
ret="0"
COPYDIR=${PWD%etc/cluster.d}
TARGETDIR=`dirname $COPYDIR`
HOSTLIST=$TMPDIR/hostlist

# build a list of hosts, but exclude the host we're running on

THISHOST=`hostname`
IPA=`ip a` # get the ip config of this node
> $HOSTLIST	# clear the hostlist, if any
for i in $*; do
	echo $THISHOST | grep $i &> /dev/null # should work in most cases :) [0 == match, 1==no match]
	if [ $? -eq 0 ]; then
		continue
	fi
	echo $IPA | grep $i &> /dev/null # not the name, is it by ip addr?
	if [ $? -eq 0 ]; then
		continue
	fi
	# ok, it's not this host
	echo $i >> $HOSTLIST
done

write_log "starting scp of $COPYDIR to $TARGETDIR on hosts $*"
$COPYDIR/pscp -h $HOSTLIST -r $COPYDIR $TARGETDIR &> /dev/null

if [ $? -eq 0 ]; then
	write_log "scp to hosts complete"
else
	write_log "scp failed"
	ret="255"	# if we can't copy the scripts to the other servers, we can't continue
fi


source ../postscript
