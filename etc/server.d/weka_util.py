#! /usr/bin/python

import sys
import os
import re
import pdb
import socket
import threading
import platform
import time
import json
from threading import Thread
from time import gmtime, strftime
import pip
import plumbum
import argparse
import math
import random
import logging.config
from plumbum import local, FG
from plumbum import SshMachine
from traceback import print_exc

class weka_thread(threading.Thread):
    def __init__(self, thread_id, name, ip, thread_type, **kwargs):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.ip = ip
        self.thread_type = thread_type
        self.kwargs = {}
        for key, value in kwargs.iteritems():
            self.kwargs[key] = value

    def run(self):
        if self.thread_type == 'deploy':
            self.weka_yum_install(self.ip)
        elif self.thread_type == 'get_host_info':
            self.host_info = self.get_all_host_info(self.ip, self.kwargs)
        elif self.thread_type == 'run_cmd':
            self.run_cmd(self.ip, self.kwargs)

    def weka_yum_install(self, ip):
        logger.info('- Deploying weka PACKAGES (RPM/DEB) on host %s ', ip)
        rem = SshMachine(ip)
        s = rem.session()
        for nic_type in weka_info[ip]['nic_types'].keys():
            if weka_info[ip]['nic_types'][nic_type]:
                if nic_type == 'ixgbe' or nic_type == 'ixgbevf':
                    output = s.run('yum install -y weka-netpack-ixgbevf.x86_64')
                    logger.info('%s --> \n%s', ip, output[1])
                    output = s.run('rmmod ixgbe; sleep 3;modprobe ixgbe')
                    logger.info('%s --> \n%s', ip, output[1])
                elif nic_type == 'mlx5_core':
                    output = s.run('yum install -y weka-netbase')
                    logger.info('%s --> \n%s', ip, output[1])
        output = s.run('chkconfig netvf on')
        logger.info('%s --> \n%s', ip, output[1])
        output = s.run('service netvf start')
        logger.info('%s --> \n%s', ip, output[1])
        output = s.run('yum install -y weka-service')
        logger.info('%s --> \n%s', ip, output[1])
    
    def get_all_host_info(self, ip, kwargs):
        host_info = {}
        #add all of the kwarg keys that were passed - should be at least SSDs and NICs
        for key, value in kwargs.iteritems():
            host_info[key] = value
        try:
            rem = SshMachine(ip)
            s = rem.session()
        except Exception, e:
            logger.info('*** Error connecting to host %s - Exiting', ip)
            os._exit(1)

        host_info['os'] = s.run('lsb_release -a')[1]
        host_info['ipAddressList'] = s.run('ip address list')[1]
        nic_types = {}
        pattern = re.compile(ur'(ixgbe|ixgbevf|mlx5_core).*',re.DOTALL)
        for nic in host_info['nics_to_use']:
            try:
                ethtool = s.run('ethtool -i %s' % nic)[1]
                if ethtool:
                    nic_type = pattern.search(ethtool)
                    if nic_type:
                        nic_types[nic_type.group(1)] = True
            except:
                pass
        host_info['nic_types'] = nic_types
        try:
            host_info['watchdog'] = s.run('if [ -c /dev/watchdog ];then true;else false;fi')
        except:
            host_info['watchdog'] = False
        logger.info('- %s Search for weka packages', ip)
        host_info['yum'] = s.run('yum search weka-node')
        logger.info('- %s Get CPU info', ip)
        host_info['cpuinfo'] = s.run('lscpu')[1]
        logger.info('- %s Get memory info', ip)
        host_info['meminfo'] = s.run('cat /proc/meminfo')[1]
        logger.info('- %s Get Kernel version', ip)
        host_info['uname'] = s.run('uname -a')[1]
        logger.info('- %s Get lspci -v', ip)
        host_info['lspci'] = s.run('lspci -v')[1]
        logger.info('- %s Get DMESG', ip)
        host_info['dmesg'] = s.run('dmesg')[1]
        logger.info('- %s Get puppet status', ip)
        try:
            host_info['puppet'] = None
            host_info['puppet'] = s.run('chkconfig|grep -i puppet')[1]
        except:
            pass
        
        logger.info('- Get grub version %s', ip)
        try:
            host_info['grub'] = s.run('grub --version')[1]
        except:
            pass

        return host_info

    def run_cmd(self, ip, kwargs):
        rem = SshMachine(ip)
        s = rem.session()
        cmd = kwargs['cmd']+'||true'
        try:
            output = s.run(cmd)
            logger.info('\n')
            logger.info('%s --> \n%s', ip, output[1])
        except plumbum.commands.processes.ProcessExecutionError:
            logger.info('Failed to run on %s', ip)

def convert_hosts_range_to_ip_range(hostname_prefix, hostname_range):
    hosts = []
    hosts_ips = []
    if hostname_prefix == hostname_range:
        hosts.append(hostname_prefix)
    else:
        first_host_suffix, last_host_suffix = hostname_range.split('-')
        for suffix in range(int(first_host_suffix), int(last_host_suffix) + 1):
            hosts.append(hostname_prefix + str(suffix))
    for host in hosts:
        try:
            ip = socket.gethostbyname(host)
            if ip != '0.0.0.0':
                hosts_ips.append(ip)
        except socket.gaierror:
            logger.info('*** Could not resolve hostname %s', host)

    return hosts_ips

def get_ip_address_range(startIP, endIP):
    pat = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    ipaddress = pat.match(startIP)
    if not ipaddress:
        hostname_prefix = startIP
        hostname_range = endIP
        hosts_ips = convert_hosts_range_to_ip_range(hostname_prefix, hostname_range)
    else:
        hosts_ips = (startIP, endIP)

    return hosts_ips

def get_weka_target_hosts(hosts):
    ip_range = []
    if hosts:
        hosts_list = hosts.split(',')
        for host in hosts_list:
            try:
                startIP, endIP = host.split(':')
            except:
                startIP = host
                endIP = startIP

            curr_ip_range = get_ip_address_range(startIP, endIP)
            for ip in curr_ip_range:
                ip_range.append(ip)
            logger.info(ip_range)
    else:
        while True:
            hosts = raw_input('Enter the target weka hosts IPs (x.x.x.x:x.x.x.x.y OR hostname:x-y): ')
            if not hosts and ip_range:
                break
            hosts_list = hosts.split(',')
            for host in hosts_list:
                try:
                    startIP, endIP = host.split(':')
                except:
                    startIP = host
                    endIP = startIP
                curr_ip_range = get_ip_address_range(startIP, endIP)
                for ip in curr_ip_range:
                    ip_range.append(ip)
            logger.info(ip_range)

    return ip_range

def get_nics():
    nic_range = []
    while True:
        nics = raw_input('Enter NICs that will be used for virtual functions e.g. eth:0-1 : ')
        if not nics and nic_range:
            break
        try:
            nic_type, nic_numbers = nics.split(':')
            start_eth, end_eth = nic_numbers.split('-')
            if start_eth:
                curr_eths_range = range(int(start_eth), int(end_eth) + 1)
                for nics in curr_eths_range:
                    nic_range.append(nic_type + str(nics))
        except:
            if nics:
                nic_range.append(nics)

        logger.info(nic_range)
    return nic_range

def get_weka_ssds():
    ssds_range = []
    while True:
        ssds = raw_input('Enter the SSDs that will be dedicated for weka e.g. sd:b-f : ')
        if not ssds and ssds_range:
            break
        try:
            ssds_prefix, ssds_devs = ssds.split(':')
            start_dev, end_dev = ssds_devs.split('-')
            if start_dev:
                curr_ssds_range = xrange(ord(start_dev), ord(end_dev)+1)
                for ssd in curr_ssds_range:
                    ssds_range.append(ssds_prefix + chr(ssd))
        except:
            ssds_range.append(ssds)

        logger.info(ssds_range)
    return ssds_range

def perform_on_all_hosts(hosts_ips, thread_type, **kwargs):
    threads = {}
    thread_id = 0
    for ip in hosts_ips:
        thread_id += 1
        thread_name = 'thread-%s' % ip
        threads[ip] = weka_thread(thread_id, thread_name, ip, thread_type, **kwargs)
        threads[ip].start()
    logger.info('-- Waiting for all hosts to complete operation')
    while True:
        active_threads = 0
        for thread in threads:
            if threads[thread].isAlive():
                active_threads += 1
        if active_threads == 0:
            break
    logger.info('\n')

    return threads

def search_replace_in_file(ip, filename, fromtext, totext):
    rem = SshMachine(ip)
    s = rem.session()
    s.run("sed -i 's?%s?%s?' %s" % (fromtext, totext, filename))

def disable_selinux(hosts_ips):
    enabled_string = 'SELINUX=enforcing'
    disabled_string = 'SELINUX=disabled'
    selinux_config = "/etc/selinux/config"

    for ip in hosts_ips:
        logger.info('- Disable SELINUX on host %s\n', ip)
        search_replace_in_file(ip, selinux_config, enabled_string, disabled_string)
        rem = SshMachine(ip)
        s = rem.session()
        try:
            s.run('setenforce 0')
        except:
            pass

def update_iptables(hosts_ips):
    iptables_conf = '/etc/sysconfig/iptables'
    temp_iptables_conf = '/tmp/iptables'
    temp_new_iptables_conf = '/tmp/new_iptables'
    weka_ports_range = '14000:14100'

    for ip in hosts_ips:
        logger.info('- Update iptables on host %s\n', ip)
        iptables_weka_defined = False

        rem = SshMachine(ip)
        s = rem.session()
        rem.download(iptables_conf, temp_iptables_conf)
        iptable_defs = open(temp_iptables_conf, 'r')
        new_defs = []
        for line in iptable_defs.readlines():
            if weka_ports_range in line:
                iptables_weka_defined = True
                break
            if not line.startswith('COMMIT'):
                new_defs.append(line)

        if not iptables_weka_defined:
            f = open(temp_new_iptables_conf, 'w')
            for line in new_defs:
                if '--dport 22' in line:
                    f.write(line)
                    f.write('-A INPUT -m state --state NEW -m tcp -p tcp --dport 14000:14100 -j ACCEPT\n')
                    f.write('-A INPUT -m state --state NEW -m udp -p udp --dport 14000:14100 -j ACCEPT\n')
                else:
                    f.write(line)

            f.write('COMMIT\n')
            f.close()
            rem.upload(temp_new_iptables_conf, iptables_conf)

        s.run('service iptables stop')
        s.run('chkconfig iptables off')

def configure_network(hosts_ips, nics_to_use):
    RHEL_NIC_FILE_PATH='/etc/sysconfig/network-scripts/ifcfg-'
    for ip in hosts_ips:
        rem = SshMachine(ip)
        s = rem.session()
        rem.upload('/opt/wekaps/misc/netvf','/etc/init.d')
        if nics_to_use:
            for nic in nics_to_use:
                if not os.path.isfile('%s%s' % (RHEL_NIC_FILE_PATH,nic)):
                    logger.info('- Host %s NIC %s configuration file created', ip, nic)
                    s.run("echo 'DEVICE=%s' > /etc/sysconfig/network-scripts/ifcfg-%s" % (nic, nic))
                    s.run("echo 'TYPE=Ethernet' >> /etc/sysconfig/network-scripts/ifcfg-%s" % nic)
                    s.run("echo 'ONBOOT=yes' >> /etc/sysconfig/network-scripts/ifcfg-%s" % nic)
                    s.run("echo 'NM_CONTROLLED=no' >> /etc/sysconfig/network-scripts/ifcfg-%s" % nic)
                    s.run("echo 'MTU=9000' >> /etc/sysconfig/network-scripts/ifcfg-%s" % nic)
        else:
            logger.info('*** No NICs to configure\n')

def configure_hosts(hosts_ips, nics_to_use):
    disable_selinux(hosts_ips)
    try:
        update_iptables(hosts_ips)
    except:
        pass
    configure_network(hosts_ips, nics_to_use)

def success_post_deploy():
    logger.info('''
    ----------------------------------------------------------------------
    Great SUCCESS: Use chrome to connect to port 14005 on any of the hosts to initialise the weka cluster
    ----------------------------------------------------------------------
    ''')

def success_post_verify():
    logger.info('''
    ----------------------------------------------------------------------
    Great SUCCESS: The environment is ready for WekaIO deployment
    ----------------------------------------------------------------------
    ''')

def failed_post_deploy():
    logger.info('''
    ------------------------------------------------------------------------------------------
    ALAS: Environment is not ready for weka deployment
    Please review readiness report to prepare the env for weka deployment then run once again
    ------------------------------------------------------------------------------------------
    ''')

def verify_host_connectivity(hosts_ips):
    root_connectivity = True
    for ip in hosts_ips:
        try:
            rem = SshMachine(ip)
            s = rem.session()
            remote_username = s.run('whoami')
            if not 'root' in remote_username[1]:
                logger.info('*** No root user connectivity to -> %s', ip)
                root_connectivity = False
        except Exception, e:
            logger.info('type is: %s', e.__class__.__name__)

    return root_connectivity

def check_puppet_status(ip, weka_info):
    logger.info('- Checking puppet status')
    puppet_disabled = True
    if weka_info[ip]['puppet']:
        puppet_disabled = False
        logger.info('*** Host %s has puppet instaled - please uninstall ', ip)

    return puppet_disabled

def check_yum_repo_connectivity(ip, weka_info):
    logger.info('- Checking weka yum repository connectivity')
    weka_packages_accessible = True
    pattern = re.compile(ur'(Matched)',re.UNICODE)
    rpm_found = pattern.search(weka_info[ip]['yum'][1])
    if not rpm_found:
        weka_packages_accessible = False
        logger.info('*** Host %s has no access to weka yum repository ', ip)

    return weka_packages_accessible

def check_grub_iommu(ip, weka_info):
    logger.info('- Checking DMESG for IOMMU in GRUB')
    iommu_not_found = True
    pattern = re.compile(ur'(intel_iommu=on)',re.UNICODE)
    iommu_found = pattern.search(weka_info[ip]['dmesg'])
    if iommu_found:
        iommu_not_found = False
        logger.info('*** Host %s GRUB contains intel_iommu=on ', ip)

    return iommu_not_found

def check_watchdog_enabled(ip, weka_info):
    logger.info('- Checking HW Watchdog is enabled')
    watchdog_enabled = True
    if not weka_info[ip]['watchdog']:
        watchdog_enabled = False
        logger.info('*** Host %s does not have  HW watchdog enabled ', ip)

    return watchdog_enabled

def check_nic_type(ip, weka_info):
    logger.info('- Checking NICs Drivers/OFED compatibility ')
    rem = SshMachine(ip)
    s = rem.session()
    correct_nic_config = True
    for nic in weka_info[ip]['nics_to_use']:
        nic_ethtool = s.run('ethtool -i %s' % nic)
        pattern = re.compile(ur'(driver: )(ixgbe|ixgbevf|mlx5_core).*(version: )(4.0.1|4.3.15|3.3-1.0.4|3.4-1.0.6)',re.DOTALL)
        nic_driver_version = pattern.search(nic_ethtool[1])
        if nic_driver_version and (nic_driver_version.group(2) == 'ixgbevf'):
            if nic_driver_version.group(4) != '3.4.3-weka':
                logger.info('*** Host %s Intel Virtual Function NIC %s does not have driver 3.4.3-weka installed - It will be updated by weka', ip, nic)    
        if nic_driver_version and (nic_driver_version.group(2) == 'ixgbe'):
            if nic_driver_version.group(4) != '4.3.15':
                logger.info('*** Host %s Intel NIC %s not have driver 4.3.15 installed - It will be updated by weka', ip, nic)    
        elif nic_driver_version and (nic_driver_version.group(2) == 'mlx5_core'):
            if nic_driver_version.group(4) != '3.3-1.0.4' and nic_driver_version.group(4) != '3.4-1.0.6':
                correct_nic_config = False
                logger.info('*** Host %s Mellanox NIC %s not have OFED 3.3-1.0.4 or 3.4-1.0.6 installed ', ip, nic)    
        else:
            correct_nic_config = False
            logger.info('*** Host %s does not have a supported NIC ', ip)

    return correct_nic_config

def check_ssds_readiness(ip, weka_info):
    devPrefix = '/dev/'
    rem = SshMachine(ip)
    s = rem.session()
    ssds_correct = True
    for ssd in weka_info[ip]['weka_ssds']:
        try:
            dev = s.run('ls %s%s' % (devPrefix,ssd))
        except:
            ssds_correct = False
            logger.info('*** Can not find /dev/%s', ssd)

    return ssds_correct

def check_os_type(ip, weka_info):
    logger.info('- Checking for Compatible OS on all hosts')
    correctOsType = True
    pattern = re.compile(ur'(CentOS|Ubuntu)',re.UNICODE)
    hostOS = pattern.search(weka_info[ip]['os'])
    if not hostOS:
        correctOsType = False
        logger.info('*** Host %s does not have compatible OS ', ip)

    return correctOsType

def check_cpu_type(ip, weka_info):
    logger.info('- Checking for INTEL CPU')
    pattern = re.compile(ur'(GenuineIntel)',re.UNICODE)
    if pattern.search(weka_info[ip]['cpuinfo']):
        intel_cpu = True
    else:
        intel_cpu = False
        logger.info('*** Host %s does not have an Intel CPU ', ip)

    return intel_cpu

def check_mem_size(ip, weka_info):
    logger.info('- Checking Minimum 12GB RAM size')
    correct_mem_size = False
    required_mem_size_kb = 12582912
    meminfo = weka_info[ip]['meminfo'].split('\n')
    for mem in meminfo:
        matched = re.search(r'^MemTotal:\s+(\d+)', mem)
        break
    if matched:
        mem_total_kb = int(matched.groups()[0])
        if mem_total_kb >= required_mem_size_kb:
            correct_mem_size = True

    if not correct_mem_size:
        logger.info('*** Host %s RAM is smaller than 12GB ', ip)

    return correct_mem_size

def check_hyper_threading(ip, weka_info):
    logger.info('- Checking that Hyper Threading is disabled')
    pattern = re.compile(ur'(per core:)\s*(\d+)',re.UNICODE)
    threads = pattern.search(weka_info[ip]['cpuinfo'])
    if threads and threads.group(2) == '1':
        hc_disabled = True
    else:
        hc_disabled = False
        logger.info('*** Host %s Hyper threading is not disabled ', ip)

    return hc_disabled

def convert_data_to_dict(hosts):
    weka_hosts = {}
    for key in hosts.keys():
        weka_hosts[key] = {}
        for cardo in hosts[key].host_info.keys():
            weka_hosts[key][cardo] = hosts[key].host_info[cardo]

    return weka_hosts

def verify_env_readiness(weka_info):
    logger.info('''
    -----------------------
    Verifying env readiness
    -----------------------
    ''')
    is_env_ready = False
    if verify_host_connectivity(hosts_ips):
        is_env_ready = True
        for ip in weka_info.keys():
            logger.info('[Testing host %s]', ip)
            is_env_ready = is_env_ready & check_puppet_status(ip, weka_info)
            is_env_ready = is_env_ready & check_yum_repo_connectivity(ip, weka_info)
            is_env_ready = is_env_ready & check_nic_type(ip, weka_info)
            is_env_ready = is_env_ready & check_ssds_readiness(ip, weka_info)
            is_env_ready = is_env_ready & check_grub_iommu(ip, weka_info)
            is_env_ready = is_env_ready & check_os_type(ip, weka_info)
            is_env_ready = is_env_ready & check_cpu_type(ip, weka_info)
            is_env_ready = is_env_ready & check_mem_size(ip, weka_info)
            is_env_ready = is_env_ready & check_hyper_threading(ip, weka_info)
            if weka_info[ip]['boxMode']:
                is_env_ready = is_env_ready & check_watchdog_enabled(ip, weka_info)

    return is_env_ready

def disable_irq_balance(weka_info):
    for ip in weka_info.keys():
        rem = SshMachine(ip)
        s = rem.session()
        try:
            if s.run('chkconfig |grep -i irqbalance'):
                logger.info('- Disable irqbalance service on server %s', ip)
                s.run('service irqbalance stop')
                s.run('chkconfig irqbalance off')
        except:
            pass

def get_weka_box_mode(isBoxMode):
    if isBoxMode:
        boxMode = isBoxMode
    else:
        boxMode = raw_input('Is this a BOX Mode installation (Y/N)[N]:')
        while (boxMode != 'Y') and (boxMode != 'N') and (boxMode != 'y') and (boxMode != 'n') and (boxMode != ''):
            boxMode = raw_input('Is this a BOX Mode installation (Y/N)[N]:')
        if boxMode == 'Y' or boxMode == 'y':
            boxMode = True
        else:
            boxMode = False

    return boxMode

def get_weka_dataplane_ips(required_num_dp_ips):
    logger.info('- Requires %s Dataplane IPs for ip jumbo frame connectivity test' % required_num_dp_ips)
    ip_range = []
    while len(ip_range) < required_num_dp_ips:
        while True:
            hosts = raw_input('Enter the WekaIO dataplane IPs (x.x.x.:x-y OR hostname:x-y): ')
            if not hosts and ip_range:
                break
            try:
                startIP, endIP = hosts.split(':')
            except:
                startIP = hosts
                endIP = startIP

            curr_ip_range = get_ip_address_range(startIP, endIP)
            if startIP == endIP:
                ip_range.append(startIP)
            else:
                for ip in curr_ip_range:
                    ip_range.append(ip)
            logger.info(ip_range)

    return ip_range

def configure_dataplane_ips(hosts, nics_to_use, dataplane_ips):
    MTU = 9000
    SUBNET=24
    available_ips = []
    used_ips = {}
    j = 0
    while len(dataplane_ips) < (len(hosts) * len(nics_to_use)):
        required_num_dp_ips = (len(hosts) * len(nics_to_use))
        logger.info('*** Not enough dataplane IPs for connectivity test')
        dataplane_ips = get_weka_dataplane_ips(required_num_dp_ips)
    pattern = re.compile(ur'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})',re.DOTALL)
    for ip in hosts:
        used_ips[ip] = []
        rem = SshMachine(ip)
        s = rem.session()
        for nic in nics_to_use:
            ipAddrList = s.run('ip address list %s' % nic)[1]
            ipAddress = pattern.search(ipAddrList)
            if ipAddress:
                logger.info('Using Current Host %s nic %s IP - %s', ip, nic, ipAddress.group(1))
                used_ips[ip].append(ipAddress.group(1))
            else:
                logger.info('Configure Host %s nic %s IP - %s', ip, nic, dataplane_ips[j])
                s.run('ip address add %s/%s dev %s' % (dataplane_ips[j],SUBNET,nic))
                s.run('ip link set %s mtu %s' % (nic,MTU))
                used_ips[ip].append(dataplane_ips[j])
                j += 1
    return used_ips

def unconfigure_dataplane_ips(hosts, nics_to_use):
    logger.info('Unconfiguring the dataplane IPs')
    for ip in hosts:
        rem = SshMachine(ip)
        s = rem.session()
        for nics in nics_to_use:
            s.run('ifconfig %s 0.0.0.0' % nics)

def check_full_connectivity(ips, dataplane_ips_used, nics_to_use):
    fullConnectivity = True
    for host_ip in ips:
        rem = SshMachine(host_ip)
        s = rem.session()
        s.run('echo 2 > /proc/sys/net/ipv4/conf/default/rp_filter')
        s.run('echo 2 > /proc/sys/net/ipv4/conf/all/rp_filter')
        for nics in nics_to_use:
            for r_ip in dataplane_ips_used.keys():
                for r_dp_ip in dataplane_ips_used[r_ip]:
                    if not [x for x in dataplane_ips_used[host_ip] if x == r_dp_ip]: #this verifies that we do not ping IPs on other ports on the same host
                        try:
                            s.run('ping -I %s -s 8972 -M do -c 1 %s' % (nics, r_dp_ip))
                            logger.info('- [%s] ping -I %s -s 8972 -M do -c 1 %s - ACK', host_ip, nics, r_dp_ip)
                        except plumbum.commands.processes.ProcessExecutionError:
                            fullConnectivity = False
                            logger.info('*** No Jumbo frame connectivity from [%s] %s to %s', ip, nics, r_dp_ip)
                        except:
                            logger.info('Call Support')
                            fullConnectivity = False

    if not fullConnectivity:
        logger.info('*** Connectivity testing failed - can not deploy')

    return fullConnectivity

if __name__ == '__main__':
    logger = logging.getLogger('weka_util')
    timestr = time.strftime("%m%d%Y-%H%M%S")
    hdlr = logging.FileHandler('/tmp/weka_util.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    consoleFormatter = logging.Formatter('%(message)s')
    console = logging.StreamHandler()
    console.setFormatter(consoleFormatter)
    logger.addHandler(console)
    logger.setLevel(logging.INFO)
    console.setLevel(logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--test_only',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Test environment for weka readiness')
    parser.add_argument('-d', '--deploy',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Test environment and deploy weka')
    parser.add_argument('-fd', '--force_deploy',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Will try to deploy even if environment test failed')
    parser.add_argument('-b', '--box',
                        required=False,
                        action='store_true',
                        default=False,
                        help='This a BOX Mode installation')
    parser.add_argument('-r', '--run_cmd',
                        required=False,
                        help='Run command on all hosts')
    parser.add_argument('-c', '--clients',
                        required=False,
                        default=None,
                        help='IP Addresses to run on')
    parser.add_argument('-f', '--file',
                        required=False,
                        help='Prints weka_info configuration as loaded from file')

    args = parser.parse_args()
    cmd = args.run_cmd
    weka_data_file = args.file
    logger.info(' %s --- START --- ' % timestr)
    if not args.test_only and not args.deploy and not cmd:
        logger.info('No action has been selected please rerun with --help for list of actions')
        exit(1)

    os.system('clear')
    if weka_data_file: # Check if user chose to use a data file that contains all of the configuration already
        try:
            with open('%s' % weka_data_file) as weka_input:
                weka_info = json.load(weka_input)
        except:
            logger.info('Could not load json file')
            exit(1)
        hosts_ips = weka_info.keys()
        nics_to_use = weka_info[weka_info.keys()[0]]['nics_to_use']
        weka_ssds = weka_info[weka_info.keys()[0]]['weka_ssds']
        boxMode = weka_info[weka_info.keys()[0]]['boxMode']
        dataplane_ips_from_json = weka_info[weka_info.keys()[0]]['dataplane_ips_used']
    elif args.test_only or args.deploy: # If no data file provided then we need to get the info from the customer
        hosts_ips = get_weka_target_hosts(args.clients)
        nics_to_use = get_nics()
        weka_ssds = get_weka_ssds()
        boxMode = get_weka_box_mode(args.box)
        required_num_dp_ips = (len(hosts_ips) * len(nics_to_use))
        dataplane_ips = get_weka_dataplane_ips(required_num_dp_ips)
        dataplane_ips_from_json = None
    elif args.run_cmd:
        hosts_ips = get_weka_target_hosts(args.clients)

    if args.test_only or args.deploy:
        logger.info('- Connecting to all hosts to gather information')
        hosts = perform_on_all_hosts(hosts_ips, 'get_host_info', weka_ssds=weka_ssds, nics_to_use=nics_to_use)
        weka_info = convert_data_to_dict(hosts)

        if dataplane_ips_from_json:
            used_ips = []
            for key in dataplane_ips_from_json.keys():
                for ip in dataplane_ips_from_json[key]:
                    used_ips.append(ip) 
            dataplane_ips_used = configure_dataplane_ips(hosts, nics_to_use, used_ips) 
        else:
            dataplane_ips_used = configure_dataplane_ips(hosts, nics_to_use, dataplane_ips)

        for ip in weka_info.keys():
            weka_info[ip]['nics_to_use'] = nics_to_use
            weka_info[ip]['weka_ssds'] = weka_ssds
            weka_info[ip]['dataplane_ips_used'] = dataplane_ips_used
            weka_info[ip]['boxMode'] = boxMode

        env_ready = verify_env_readiness(weka_info) # Going over all the details and verifying hosts' readiness
        
        jumbo_connectivity = True
        if args.force_deploy:
            logger.info('- FORCE DEPLOY')
        else:
            logger.info('[- Testing IP Connectivity]')
            if not check_full_connectivity(hosts_ips, dataplane_ips_used, nics_to_use):
                jumbo_connectivity = False

        with open('/tmp/weka_info.json', 'w') as outfile:
            json.dump(weka_info, outfile)

        if (env_ready and jumbo_connectivity) or args.force_deploy:
            if args.deploy:
                if boxMode: # If box mode then disable irqbalance service (if exists)
                    disable_irq_balance(weka_info)
                configure_hosts(hosts_ips,nics_to_use)
                perform_on_all_hosts(hosts_ips,'deploy')
                success_post_deploy()
            else:
                success_post_verify()
        else:
            failed_post_deploy()
    elif cmd:
        perform_on_all_hosts(hosts_ips, 'run_cmd', cmd=cmd)
    timestr = time.strftime("%m%d%Y-%H%M%S")
    logger.info(' %s --- FIN --- ' % timestr)

