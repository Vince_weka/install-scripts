#!/bin/bash

DESCRIPTION="Make sure required packages are installed on all nodes"
source ../preamble

PACKAGES="elfutils-libelf-devel \
glibc \
glibc-headers \
glibc-devel \
gcc \
make \
perl \
rpcbind \
pciutils \
gtk2 \
atk \
cairo \
gcc-gfortran \
tcsh \
lsof \
tcl \
tk \
sysstat \
strace \
ipmitool \
tcpdump \
telnet \
nmap \
net-tools \
dstat \
numactl \
numactl-devel \
python \
automake \
libaio \
libaio-devel \
perl \
lshw \
hwloc \
pciutils \
lsof \
wget \
bind-utils \
vim-enhanced \
nvme-cli \
nfs-utils \
initscripts \
screen \
tmux \
git \
epel-release \
sshpass \
python-pip \
lldpd \
bmon \
nload \
pssh \
pdsh \
iperf \
fio \
htop"

# make sure epel-release and pdsh/pssh are installed here first...
write_log "Installing epel-release on this node"
yum -y install epel-release &> /tmp/epel-install.log
if [ $? -ne 0 ]; then
	write_log "Unable to install epel-release on this host.  See /tmp/epel-install.log for details."
	exit 255
fi
write_log "Installing pdsh & pssh on this node"
yum -y install pdsh pssh &> /tmp/package-install.log
if [ $? -ne 0 ]; then
	write_log "Unable to install pdsh & pssh on this host.  See /tmp/package-install.log for details."
	exit 255
fi

# pdsh needs a comma-separated list of hosts
pdsh_hosts=`echo $* | sed 's/ /,/g'`

write_log "Installing epel-release"
pdsh -w $pdsh_hosts yum -y install epel-release \&\> /tmp/epel-install.log
if [ $? -ne 0 ]; then
	echo "At least one node failed install of epel-release - check /tmp/epel-install.log on all the nodes"
	exit 255
fi

write_log "Installing packages"
pdsh -w $pdsh_hosts yum -y install $PACKAGES \&\> /tmp/packages-install.log
if [ $? -ne 0 ]; then
	echo "At least one node failed install of packages - check /tmp/packages-install.log on all the nodes"
	exit 255
fi

source ../postscript
