#!/bin/bash

DESCRIPTION="Verify timesync"
source ../preamble


# Put your stuff here
ret="0"
for i in $*
do
	DIFF=`clockdiff $i | awk '{ print $2 + $3 }'`
	if [ $DIFF -lt 0 ]; then let DIFF="(( 0 - $DIFF ))"; fi
	echo "Diff is $DIFF"
	if [ $DIFF -gt 2 ]; then 
		write_log "Host $i is not in timesync"
		ret="1"
	else
		write_log "Host $i timesync ok"
	fi
done

source ../postscript
